# output dir -> test dir -> keepstate

_init_test () {
	QHOME=$TD/qemu # var for cleanup use
	V="./vm.sh -q $QHOME -n main"
	$V start
}

_clean_test () {
	$V stop; echo; $V destroy; $V clean; $V silence
	test -d $QHOME/vm/main && rm -rf $QHOME
}

__test_list () {
	$V list | cut -d\| -f2 | sort
}

__test_available () {
	$V available | sort
}

__test_conf () {
	$V conf all one
	$V conf dump one
	$V conf remove one
}

__test_self () {
	$V self cmd echo from self test: $PWV
	$V self module net
}

__test_stats () {
	$V stats
}

__test_module () {
	$V module net
	$V module disk
}

__test_cmd () {
	$V cmd hostname
	$V cmd ip addr
}

