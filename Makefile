PREFIX := ${HOME}
SH_BIN := ${PREFIX}/bin
SH_SHARE := ${PREFIX}/share
bin := vm.sh
components := module type composite
lib=./shlib/.*.sh


install:
	@mkdir -p ${SH_SHARE} ${SH_BIN}
	@cp -ra ${components} ${SH_SHARE}
	@cp -a ${lib} ${SH_SHARE}
	@cp ${bin} ${SH_BIN}
	@sed -i -e 's#@sharedir@#${SH_SHARE}#' \
			-e 's#@bindir@#${SH_BIN}#' ${SH_BIN}/vm.sh
	@cd ${SH_SHARE}/module && ln -sf ../.func.sh && ln -sf ../.msg.sh
	@echo installed to ${PREFIX}

clean:
	@echo do some cleaning
    
.PHONY: install clean
