_set_perms () { # dir -> username -> Monad Perms
	chmod 700 $1
	find $1 -type f -print0 | xargs -0r chmod 600
	chown -R $2:$2 $1
}

_set_authorized () { # uid 
	tf=$(mktemp -p $tmp)
	cat $SHOME/authorized_keys $udir/$1/pubkey $udir/$root_id/pubkey \
		$udir/$1/pubkey | sort -u >$tf
	cp $tf $SHOME/authorized_keys
	_set_perms $SHOME $USERNAME
}

_process () { 
	uid=$1; shift
	USERNAME=$(cat $udir/$uid/name) || return 1
	SHOME="/home/$USERNAME/.ssh"; mkdir -p "$SHOME"
	test ! -f $SHOME/authorized_keys && touch $SHOME/authorized_keys
	: ${cmd:=${1:-info}}; test -n "$1" && shift
	case "$cmd" in
	test) echo uid: $uid, name: $USERNAME ;;
	auth) _set_authorized $uid ;;
	add) 
		adduser -D $USERNAME || return 1
		passwd -u $USERNAME || return 1
		_set_authorized $uid
		;;
	group)
		addgroup $1
		addgroup $USERNAME $1
		;;
	home) getent passwd $USERNAME | cut -d: -f6 ;;
	esac
}

udir=$HOME/.qtree/$run_id/mod/users
root_id=$(_search_id $udir name '^root$')
for uid in $(_search_id $udir $fname "$name")
do _process $uid "$@"; done

