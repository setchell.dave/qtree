printf '%0.3d\n' 0 $(seq 26) >$tmp/i
printf '%c\n' a b c d e f g h i j k l m n o p q r s t u v w x y z >$tmp/a
paste -d: $tmp/i $tmp/a >$tmp/d.map

_process () {
	did=$1; ddata=$2; shift; shift
	: ${cmd:=${1:-stats}}; test -n "$1" && shift
	case "$cmd" in
	test) echo did: $did device: $device ;;
	mount) # disk num -> mount point
		test -f $ddata/mountpoint && mount=$(cat $ddata/mountpoint)
		: ${mount:=/mnt/tmp}; mkdir -p $mount
		mount $device $mount
		;;
	umount) mount $device ;;
	format)
		blkid -o device $device >/dev/null 2>&1 && { echo $device already formatted; return 1 ;}
		mke2fs -t ext4 -L $did $device
		;;
	esac
}

ddir=$HOME/.qtree/$run_id/vms/$vid/drive
for dnum in $(_search_id $ddir "$fname" "$name")
do
	test $? -eq 0 || exit 1
	letter=$(grep "^${dnum}:" $tmp/d.map); letter=${letter#*:}
	device=/dev/vd${letter}
	test -d $ddir/$dnum || { echo no drive $device on $vid; continue ;}
	did=$(cat $ddir/$dnum/image_id)
	_process $did $ddir/$dnum "$@"
done

