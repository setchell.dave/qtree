#!/bin/sh
QTREE_VERSION=1
. ${SH_SHARE}/type/.init.sh
CTYPE=dev/haskell

while getopts t:h arg
do
    case "$arg" in
    t) TZONE="$OPTARG" ;;   # branch 
    h) _gen_help && exit 0 ;; # this help
    esac
done; unset arg; shift $(( $OPTIND - 1 )); OPTIND=1
: ${TZONE:=US/Pacific}
: ${INIT:=false}

_init () {  # () -> Monad checked
	INIT=$INIT $V type dev -t $TZONE 
	$VR module pkg add ghc ghc-doc ghc-dev
	curl -sSL https://get.haskellstack.org/ | $V pin
}

: ${cmd:=${1:-stats}}; test -n "$1" && shift
_validate
case "$cmd" in
*) $V $cmd "$@" ;;
esac
