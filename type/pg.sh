: ${PGUSER:=${1:-postgres}}
: ${DATABASE:=${2:-postgres}}
: ${CONFDIR:=${3:-/etc/postgresql}}

NETWORK=$(cat  $HOME/.qtree/$run_id/network)

IP=$(ip -4 addr show eth0 | grep -o 'inet.*' | cut -d' ' -f2 | cut -d/ -f1)
_conn_str () {  # () -> connection-string
    port=5432
    printf 'postgresql://%s@%s:%s/%s\n' "$PGUSER" "$IP" "$port" "$DATABASE"
}

_default_reconf () {
    rc-service postgresql start
    rc-service postgresql stop
	echo breif chill
    sFmt=$(printf "s/.*listen_addresses.*=.*/listen_addresses = '%s'/" $IP)
 	sed -i "$sFmt" ${CONFDIR}/postgresql.conf
    sed -i "s#.*host.*${NETWORK}.*##" ${CONFDIR}/pg_hba.conf
	echo "host all all ${NETWORK} trust" >>${CONFDIR}/pg_hba.conf
    rc-service postgresql start
}

apk info -qe postgresql || { apk add postgresql postgresql-contrib; _default_reconf ;}
_conn_str

