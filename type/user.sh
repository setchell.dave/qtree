#!/bin/sh
QTREE_VERSION=1
. "${SH_SHARE}/type/.init.sh" 
CTYPE=user

while getopts hH:s: arg
do
    case "$arg" in
    h) _gen_help && exit 0 ;; # this help
    H) home="$OPTARG" ;;     # home dir
    s) SHL="$OPTARG" ;;      # shell
    esac
done; unset arg; shift $(( $OPTIND - 1 )); OPTIND=1

: ${SHL:=/bin/mksh}
: ${home:=/home/${UNAME}}

_load_keys () {  # Monad keyStore -> Monad sshUserAdd
    kDir=${1:-${home}/.ssh}
    { cat -; $VR module path -f -p "${kDir}/authorized_keys" dump ;} \
        | sort -u | $VR module path -f -p "${kDir}/authorized_keys" replace 
    $VR pin <<EOF
chmod 700 "${kDir}"
find ${kDir} -type f -print0 | xargs -0r chmod 600 
chown -R $UNAME:$UNAME "${kDir}"
EOF
}

_init () { # () -> Monad init user
    $VR module pkg add mksh
    $VR pin <<EOF
adduser -s "$SHL" -D -h "$home" $UNAME
printf '%s\n' foo foo | passwd $UNAME
passwd -du $UNAME
addgroup $UNAME; addgroup $UNAME $UNAME; addgroup $UNAME wheel
EOF
    cat ${SH_SHARE}/*.pub ${HOME}/.ssh/*.pub | _load_keys
    $V module path -f -H .mkshrc replace <<'EOF'
_pathmunge () {
	while test -n "$1"
	do case :$PATH: in *:$1:*):;; *) PATH="$1:$PATH" ;; esac; shift ; done
}
_pathmunge '.' "$HOME/go/bin" "$HOME/.cabal/bin" "$HOME/bin" "$HOME/r/bin"

type vis >/dev/null 2>&1 && EDITOR=vis || EDITOR=vi

_show_ps1 () {
    border='[--]'
    local date=$(date +"%a %d %b %H:%M")
    local dir=$(echo $PWD | rev | cut -c-24 | rev)
    local slug=$(printf '[ %-16.16s ]' "$(id -u -n)@$(hostname)")
    local slug_=$(printf '[ %16.16s ]' "$date")
    local slug__=$(printf '[ %-24.24s ]' "$dir")
    slug="$slug$slug_$slug__"
    printf '%s %-70.70s %s' "$border" "$slug" "$border"
}

PS1='
$(_show_ps1)
---] '

HISTFILE="${HOME}/.history.mksh"
HISTSIZE=4096
\export MANWIDTH=80 LESSHISTFILE=- EDITOR PATH HISTFILE HISTSIZE
EOF

    $V module path -f -H .profile replace <<'EOF'
GOROOT=/usr/lib/go
GOPATH=${HOME}/go
GIT_SSH_COMMAND="ssh -o StrictHostKeyChecking=no"
export GOROOT GOPATH GIT_SSH_COMMAND
EOF
}

_validate >&2
: ${cmd:=${1:-info}}; test -n "$1" && shift
case "$cmd" in
init) _init ;; # explicit init
group) $VR cmd "addgroup $1; addgroup $UNAME $1" ;; # add user to group
home) $V cmd getent passwd $UNAME | cut -d: -f6 ;; # echo user home
deactivate) $V cmd rm -rf "${home}/.ssh" ;; # deactivate user access
*) _${cmd} "$@" ;; # run function
esac


