#!/bin/sh
. ${SH_SHARE}/.func.sh
. ${SH_SHARE}/.msg.sh

VR="$V -u root"
$VR start

_validate () { # () -> bool
	sleep 2 # DUMB gives extra time for packages.start
	$INIT && $VR module path -f -p /var/qtree/typev/${CTYPE}.v remove
    rv=$($VR module path -f -p /var/qtree/typev/${CTYPE}.v dump)
    test "$QTREE_VERSION" -gt ${rv:-0} && _init && \
		{ echo $QTREE_VERSION | $VR module path -f -p /var/qtree/typev/${CTYPE}.v replace ;}
	unset rv
}

	

