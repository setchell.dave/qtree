#!/bin/sh
QTREE_VERSION=1
. ${SH_SHARE}/type/.init.sh
CTYPE=dev

while getopts t:h arg
do
    case "$arg" in
    t) TZONE="$OPTARG" ;;   # branch 
    h) _gen_help && exit 0 ;; # this help
    esac
done; unset arg; shift $(( $OPTIND - 1 )); OPTIND=1
: ${TZONE:=US/Pacific}

_init () {  # () -> Monad checked
	$VR disk-num 2
	$VR stop; $VR start
	$VR module disk -m "/home" mount
	$VR module disk -m "/usr" -d /dev/vdc persist
	INIT=$INIT $V type user home
	$VR module pkg repo community
	$VR module pkg add git build-base vis ncurses tzdata musl-dev go curl python3 postgresql-client
	$VR pin <<EOF
ln -sf /usr/bin/python3 python
cd /etc
cp /usr/share/zoneinfo/${TZONE} /etc/localtime
echo "${TZONE}" >/etc/timezone
EOF
	test -f ${HOME}/.gitconfig \
		&& $V module path -f -H .gitconfig replace <${HOME}/.gitconfig
	$V pin <<'EOF'
. ${HOME}/.profile
mkdir -p ${HOME}/repo
cd ${HOME}/repo
test ! -e gift && git clone git@gitlab.com:setchell.dave/gift.git
cd gift	
git submodule init
git submodule update --remote
make install
EOF
	test -f ${HOME}/.config/vis/visrc.lua && <${HOME}/.config/vis/visrc.lua \
		$V module path -f -H .config/vis/visrc.lua replace
}

_validate
: ${cmd:=${1:-stats}}; test -n "$1" && shift
case "$cmd" in
*) $V $cmd "$@" ;; 
esac
