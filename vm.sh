#!/bin/sh

: ${SH_SHARE:=@sharedir@}
: ${SH_BIN:=@bindir@}
test "$SH_SHARE" != ${SH_SHARE#@} && SH_SHARE=$PWD
test "$SH_BIN" != ${SH_BIN#@} && SH_BIN=$PWD
for f in $(ls ${SH_SHARE}/.*.sh)
do . $f; done

_get_or_gen () { # dir -> name -> [generator] -> value
	test $# -ge 3 || return 1
	test -d "$1" || mkdir -p "$1" || _error fail
	fname="$1/$2"; shift; shift
	test -s "$fname" || "$@" >"$fname"
	cat "$fname"
}

_mac_gen () { # () -> mac-addr
	printf 'aa:aa:%s:%s:%s:%s\n' $(_run_n 4 _r 2 hex)
}

_net_gen () {  # fmt -> network
	# avoid 0,1, and 255
	printf "${1:-10.%s.0.0/16}" $(( $(_r 10 int) % 252 + 2 ))
}

_mac () { # vmid
	_get_or_gen $QH/vms/$1 mac _mac_gen
}

_mon () { # vmid -> port
	_get_or_gen $QH/vms/$1 mon _get_port
}

_cpu () { # vmid -> cpu count
	_get_or_gen $QH/vms/$1 cpu echo 2
}

_mem () { # vmid -> mem-amt
	_get_or_gen $QH/vms/$1 mem echo 4G
}

_net () { # part -> network part
	# TODO handle subnettin'
	part="$1"; test -n "$1" && shift
	n=$(_get_or_gen $QH network _net_gen "$@")
	addr=${n%/*}; cidr=${n#*/}
	case "$part" in
	'') echo $n ;;
    nm|netmask)
		printf '%-32.32s\n' $(printf '1%0.0s' $(seq $cidr)) | tr ' ' 0 \
        	| fold -w 8 | sed 's/^/ibase=2; /' | bc | tr '\n' '.' \
        	| sed 's/\.$//'; echo
		;;
	gw) echo ${addr%.*}.$(( ${addr##*.} + 1 )) ;;
	addr|cidr) eval echo \$$part ;;
	esac
}

_taps () { # () -> taps-qemu
	tFmt="%s tap,model=virtio-net-pci,mac=%s,ifname=%s,script=no,downscript=no "
	# TODO replace w/ jsondir 
	tnmax=$(ip link | grep ^[0-9] | cut -d: -f2 | tr -d ' ' | grep ^tap \
		| tr -dc '0-9\n' | sort -nr | head -n 1); tnmax=$(( ${tnmax:-0} + 1 ))
	for t in $(ip link | grep ^[0-9] | grep "master br.q.${QID} state DOWN" \
				| cut -d: -f2 | tr -d ' ' | grep ^tap)
	do
		user=$(ip tuntap | grep ^${t} | rev | cut -d' ' -f1 | rev)
		test "$user" = $(id -u) && echo $t
	done >$tmp/taps
	nt=$(<$tmp/taps wc -l)
	nm=$(_mac $1 | wc -l)
	if test $nt -lt $nm
	then
		for _ in $(seq $(( $(_mac $1 | wc -l) - $nt )))
		do
			sudo ip tuntap add dev tap${tnmax} mode tap user $(id -u)
			sudo ip link set dev tap${tnmax} master br.q.${QID}
			echo tap${tnmax} >>$tmp/taps
			tn=$(( $tnmax +  1 ))
		done
	fi
	_mac $1 >$tmp/macs; sed -i "$(( ${nm} + 1 )),\$d" $tmp/taps
	for x in $(paste -d '^' $tmp/macs $tmp/taps)
	do sudo ip link set dev ${x#*^} up; printf "$tFmt" "-nic" ${x%^*} ${x#*^}; done
}

_idem_ipt () {  # any ole rule
    sudo iptables -C "$@" || sudo iptables -A "$@"
}

_qbase () { # * -> qemu-cmd-str ++ "*"
	id=$1; shift
	qemu-system-x86_64 -smp cpus=$(_cpu $id) -enable-kvm -cpu host \
	-device virtio-rng-pci -name process=q_${id} -m $(_mem $id) $(_taps $id) \
	-pidfile $QH/vms/$id/pid $(_disk_args $id) -monitor \
	telnet:127.0.0.1:$(_mon $id),server,nowait "$@"
}

_import_disk () { # source disk -> disk id
	test -f $1 || return 1
	# TODO local disk data to jsondir
	type=$(qemu-img info $1 | grep 'file format:' | cut -d: -f2); type=${type# *}
	did=$(_new_id $QH/images _make_id); fname=$QH/images/$did/data.${type}
	_wait cp $1 $fname
	echo $type >$QH/images/$did/type
	echo $did
}

_new_disk () { # type -> .size. -> diskid
	did=$(_new_id $QH/images _make_id); type=${1:-qcow2}; test -n "$1" && shift
	fname=$QH/images/$did/data.${type}
	qemu-img create -f $type "$fname" ${1:-5G} >/dev/null 2>&1 || return 1
	echo $type >$QH/images/$did/type
	echo free >$QH/images/$did/owner
	echo $did
}


_image_size () { # image file -> size // bytes
	qemu-img info "$1"  | grep ^virtual | cut -d'(' -f2- | cut -d' ' -f1
}

_find_disk () { # size -> id
	s=$(_norm ${1:-1}); did=''
	for did in $(_search_id $QH/images owner free)
	do
		test $(_image_size $QH/images/$did/data.*) -gt $s && { echo $did; return 0 ;}
	done
	test -z "$did" && _new_disk qcow2 $1
}

_add_drive () { # vmid -> did|size -> mountpoint -> Added drive
	test -z "$1" && return 1; did=${2:-5G}
	test -d "$QH/images/$did" || did=$(_find_disk $did)
	mountpoint=${3:-/mnt/$did}
	echo $1 >$QH/images/$did/owner
	dnum=$(_new_id $QH/vms/$1/drive _make_id add)
	echo $did >$QH/vms/$1/drive/$dnum/image_id
	echo $mountpoint >$QH/vms/$1/drive/$dnum/mountpoint
}

_new_user () { # name -> id
	tf=$(mktemp -p $tmp)
	user_name=${1:-$(id -un)}
	test -n "$(_search_id $QH/mod/users name $user_name)" && { echo $user_name already exists; return 1 ;}
	user_id=$(_new_id $QH/mod/users _make_id user)
	echo $user_name >$QH/mod/users/$user_id/name
	case $user_name in
	root) cat $QH/.ssh/*.pub >$QH/mod/users/$user_id/pubkey ;;
	*)
		test -d /home/$user_name/.ssh && \
			find /home/$user_name/.ssh -name "*.pub" -type f | xargs cat >$QH/mod/users/$user_id/pubkey
		;;
	esac
	echo $user_id
}

_add_user () { # vmid -> name -> bool 
	test -z "$1" && return 1
	for uid in $(_search_id $QH/mod/users name "$2")
	do
		udir=$QH/vms/$1/user/$(cat $QH/mod/users/$uid/name)
		test -e "$udir" && { echo user already added; return 1 ;}
		mkdir -p $udir; echo $user_id >$udir/user_id
	done
}

_disk_args  () { #
	test -d $QH/vms/$1/drive/000 || return 0 # no disk no cry
	didx=0
	for disk in $(find $QH/vms/$1/drive -name "$n3" | sort)
	do
		did=$(cat ${disk}/image_id)
		type=$(cat $QH/images/$did/type)
		printf -- '-drive file=%s,format=%s,index=%d,if=virtio ' \
			$QH/images/$did/data.${type} ${type} $didx
		didx=$(( $didx + 1 ))
	done
}

_hname () { # vmid ->
	<$QH/vms/$1/name tr -d '\n' | tr '[[:space:][:punct:]]' '-' | tr -s '[[:punct:]]'
}

_alpine_base () { # vmid -> 
	img_name=$(echo $QH/images/$(cat $QH/vms/$1/drive/000/image_id)/data.raw)
	_with loopback base "$img_name" || _error failed to enter loopback
	m=$(_cmd_loop base mountpoint); m="${m%/}/3"; test -d $m \
		|| _error failed to find base
	sudo mkdir -p $m/root/.ssh
	<${QH}/.ssh/id_rsa.pub sudo tee $m/root/.ssh/authorized_keys >/dev/null
	_hname $1 | sudo tee $m/etc/hostname >/dev/null
	sudo ln -s /etc/init.d/local $m/etc/runlevels/default/local 2>/dev/null
	sudo sed -i '/.*hostname.*/d' $m/etc/network/interfaces
	sudo sed -i '/iface.*eth0/a	hostname '"$(_hname $1)" $m/etc/network/interfaces
	printf '# -] %-55.55s[-\n' "-------" "Welcome to your instance: $name" \
		"--" | sudo tee $m/etc/motd >/dev/null
	sudo tee $m/etc/local.d/packages.start >/dev/null <<'EOF'
#!/bin/sh
netT=false
for c in bc bridge iptables util-linux tree procps iproute3 ncurses file
do
	type $c >/dev/null 2>&1 || { netT=true; apk add $c ;}
done
$netT && service networking restart
$netT && echo '%wheel ALL=(ALL) NOPASSWD: ALL' >>/etc/sudoers
EOF
	sudo chmod 744 $m/etc/local.d/packages.start
	_with_out loopback
}

_idem_create () { # vmid -> data
	if test ! -f "$QH/iso/${ISO}_base.raw"
	then
		did=$(_new_disk raw) || return 1
		_add_drive $1 $did || return 1
		</dev/tty _qbase $1 -display curses -cdrom "$QH/iso/${ISO_N}" \
			-boot d >/dev/tty 2>/dev/tty
		_alpine_base $1

		_wait cp "$QH/images/$(cat $QH/vms/$1/drive/000/image_id)/data.raw" \
			"$QH/iso/${ISO}_base.raw"
	fi
	img_name=$(echo $QH/images/$(cat $QH/vms/$1/drive/000/image_id)/data.raw)
	test ! -f "$img_name" && {
		_add_drive $1 $(_import_disk "$QH/iso/${ISO}_base.raw")
		_alpine_base $1 ;}
}

_start () { # vmid -> started 
	_running $1 || _qbase "$@"
}

_addr () { # vmid -> ip
	dig +short @$(_net gw) $(_hname $1)
}

_running () {
	test -f $QH/vms/$1/pid && kill -0 $(cat $QH/vms/$1/pid) || return 1
}

_shortname () {
	printf '%5.5s:%12.12s' $1 $(cat $QH/vms/$1/name)
}

_exec () { # vmid -> exec file -> .args. -> input
	vid=$1; fname=$2; shift; shift
	test -s $fname || return 1
	case "$1" in "-h"|"--help") _gen_help $fname; return 0 ;; esac
	run_id=$(_r 12); rt=$tmp/exec/$run_id; ip=$(_addr $vid)
	i=1;mkdir -p $rt/args
	echo ${fname%.*} >$rt/name
	while test -n "$1"
	do
		printf '%s\n' "$1" >$rt/args/$(printf '%0.3d' $i)
		printf '%s\n' $i >$rt/args.count; i=$(( $i + 1 )); shift
	done
	for send in vms network mod
	do ln -s $(_fullpath $QH/$send) $rt; done
	cat >$tmp/leader <<'EOF'
vid=$1; run_id=$2; shift; shift
rt=$HOME/.qtree/$run_id
_pre_clean () {
	test -d $rt && rm -rf $rt
}

eval set -- $(find $rt/args -type f | sort -n | xargs -r printf '"$(cat %s)" ')
while getopts n:f: arg
do
	case "$arg" in
	n) name="$OPTARG" ;;
	f) fmatch="$OPTARG" ;;
	esac
done; unset arg; shift $(( $OPTIND - 1 )); OPTIND=1
: ${name:=".*"}
: ${fname:=name}
EOF
	(cd $rt; find -L . -user $(id -u) | cpio -L -o -H newc 2>/dev/null) \
		| $SSH ${UNAME}@$ip "mkdir -p \$HOME/.qtree/$run_id; cd \$HOME/.qtree/$run_id
			cpio -iudR $UNAME 2>/dev/null"
	cat $SH_SHARE/.*.sh $tmp/leader $fname | $SSH ${UNAME}@$ip sh -s $vid $run_id 
	retval=$?
	return $retval
}

while getopts xhHi:n:fIq:tu:U arg
do
	case "$arg" in
	h) _gen_help && exit 0;;     # this help
	H) itype=hw ;;				 # hardware instead of vm
	i) ISO="$OPTARG" ;;			 # iso name
	I) INIT=true ;;
	f) FORCE=true ;;
	n) name="$OPTARG" ;;		 # vm name
	q) QH="$OPTARG"	;;			 # qemu home
	t) SSHTERM=1 ;;				 # forces ssh terminal
	x) SSHFORWARD=1 ;;			 # x11 forwarding
	u) UNAME="$OPTARG" ;;		 # sets username for connection
	U) UPDATE_HOST_KEY=true	;;	 # bool for updating host keys
	esac
done; unset arg; shift $(( $OPTIND - 1 )); OPTIND=1
: ${QH:="./qemu"}
: ${UNAME:=root}
: ${SSHTERM:=''}
: ${SSHFORWARD:=''}
: ${INIT:=false}
: ${itype:=vm}
: ${ISO:=alpine}
: ${name:=scratch}
: ${UPDATE_HOST_KEY:=false}

mkdir -p $QH/vms $QH/iso $QH/images $QH/.ssh $QH/mod/users
QH=$(_fullpath "$QH")
QID=$(_get_or_gen "$QH" id _r 10 hex)
WAN=$(ip route | grep ^default | head -1 | grep -o 'dev [^[:space:]]\+' \
	| cut -d ' ' -f2)
SSH="ssh -q -A -o StrictHostKeyChecking=no -i ${QH}/.ssh/id_rsa ${SSHTERM:+-t} ${SSHFORWARD:+-X}"
ISO_debian=https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-9.5.0-amd64-netinst.iso
ISO_alpine=http://dl-cdn.alpinelinux.org/alpine/v3.12/releases/x86_64/alpine-standard-3.12.0-x86_64.iso
ISO_DL=$(eval "echo \$ISO_${ISO}")
ISO_N=$(basename "$ISO_DL")
test -f "$QH/iso/${ISO_N}" || curl -L "$ISO_DL" >"$QH/iso/${ISO_N}" 
test 1 -ne $(cat /proc/sys/net/ipv4/ip_forward) \
	&& sudo sh -c 'echo 1 >/proc/sys/net/ipv4/ip_forward'
test -f ${QH}/.ssh/id_rsa -a -f ${QH}/.ssh/id_rsa.pub \
	|| { ssh-keygen -N '' -f ${QH}/.ssh/id_rsa; chmod 0600 ${QH}/.ssh/id_rsa ;}

test -z $(_search_id $QH/mod/users name root) && _new_user root

ip link show dev br.q.${QID} >/dev/null 2>&1 || {
	sudo ip link add name br.q.${QID} type bridge
	sudo ip addr add $(_net gw)/$(_net cidr) dev br.q.${QID} ;}
sudo ip link set dev br.q.${QID} up
_idem_ipt POSTROUTING -t nat -s $(_net) -o $WAN -j MASQUERADE
_idem_ipt FORWARD -i $WAN -o br.q.${QID} -j ACCEPT
_idem_ipt FORWARD -o $WAN -i br.q.${QID} -j ACCEPT

test -f ${QH}/dnsmasq.pid && ps -p $(cat ${QH}/dnsmasq.pid) \
	2>/dev/null 1>&2 || { 
	net=$(_net addr | cut -d. -f1,2)
cat >$QH/hosts <<EOF
${net}.0.1	qtree.${QID}.home	qtree
EOF
	cat >${QH}/dnsmasq.conf <<EOF
interface=br.q.${QID}
bind-interfaces
bogus-priv
domain-needed
no-resolv
addn-hosts=${QH}/hosts
log-facility=${QH}/dnsmasq.log
log-queries
except-interface=lo
domain=${QID}.home
server=1.1.1.1
server=8.8.8.8
dhcp-range=${net}.0.20,${net}.254.20,255.255.0.0
dhcp-option=option:router,$(_net gw)
dhcp-leasefile=${QH}/dnsmasq.leases
EOF
	sudo dnsmasq --user=root --conf-file=${QH}/dnsmasq.conf --pid-file=${QH}/dnsmasq.pid
	}

_process_no_vm () {
	: ${cmd:=${1:-stats}}; test -n "$1" && shift
	case $cmd in
	help) # all help
		for f in $(find $SH_SHARE $SH_BIN -name "*.sh" | sort -u)
		do _gen_help "$f"; echo; done
		;;
	silence|silence-full) # shut down listeners and dhcp client
		test -s ${QH}/listener && kill $(cat $QH/listener)
		upid=$(cat ${QH}/dnsmasq.pid)
		ps -p $upid 2>/dev/null 1>&2 && sudo kill -9 $upid
		test $cmd = 'silence-full' || return 0
		ip link | grep ^[0-9] | grep "master br.q.${QID} state DOWN" | cut -d: -f2 \
			| tr -d ' ' | xargs -Ixx sudo ip link del dev xx type tap
		sudo ip link del dev br.q.${QID} type bridge
    	sudo iptables -D POSTROUTING -t nat -s $(_net) -o $WAN -j MASQUERADE
    	sudo iptables -D FORWARD -i $WAN -o br.q.${QID} -j ACCEPT
    	sudo iptables -D FORWARD -o $WAN -i br.q.${QID} -j ACCEPT
		;;
	list) # shows all running vms
		test -n "$(find $QH/vms -name pid)" || return 1
		for vid in $(find $QH/vms -name pid | xargs -n1  dirname)
		do
			pid=$(cat $vid/pid); n=$(cat $vid/name)
			kill -0 $pid 2>/dev/null && echo $pid $n
		done
		;;
	available) # show all available instances in the qemu home
		find $QH/vms -name name | xargs cat
		;;
	create)
		id=$(_new_id $QH/vms _make_id)
		echo $id | _cmd_state vmset update vms; echo $name >$QH/vms/$id/name
		_idem_create $id && echo vm, $name, created || return 1
		;;
	*) DONE=false ;;
	esac
}

_process_vm () {
	vid=$1; shift
	: ${cmd:=${1:-stats}}; test -n "$1" && shift
	case "$cmd" in
	add-nic) for _ in $(seq ${1:-1}); do _mac_gen >>$QH/vms/$vid/mac ; done ;;
	add-drive) _add_drive $vid "$@" ;;
	new-user) _new_user "$@" ;;
	add-user) _add_user $vid "$@" ;;
	stats) # show details on instance
		cat $QH/vms/$vid/name
		#_addr $vid
		for disk in $(find $QH/vms/$vid/drive -name "$n3" | sort)
		do
			did=$(cat ${disk}/image_id)
			type=$(cat $QH/images/$did/type)
			qemu-img info -U $QH/images/$did/data.${type} | _box
		done
		;;
	RECONFIG) # careful
		_running $vid && { echo $(_shortname $vid) running; return 1 ;}
		_alpine_base $vid
		;;
	destroy)  # destroy instance completely
		_running $vid && { echo $(_shortname $vid) running; return 1 ;}
		{ ${FORCE:-false} || _ask "really destroy $(_shortname $vid)?" ;} \
			&& rm -rf $QH/vms/$vid
		;;
	start)
		_running $vid && { echo $(_shortname $vid) running; return 1 ;}
		_start $vid -display none -daemonize ;;
	foreground)
		_running $vid && { echo $(_shortname $vid) running; return 1 ;}
		</dev/tty _start $vid -display curses >/dev/tty ;;
	stop)  # stop instance
		_running $vid || { echo $(_shortname $vid) not running; return 1 ;}
		echo system_powerdown | nc localhost $(_mon $vid)
		for ii in $(seq 30)
		do
			_running $vid >/dev/null || break
			test $ii -ge 30 && exit 1
			printf '[%s] waiting for %s to stop' \
				$(_spin $ii 3) $(cat $QH/vms/$vid/name)
			sleep 1
			_clear
		done
		sleep 1 ## i h8 sleep. 
		;;
	test)
		_running $vid || { echo $(_shortname $vid) not running; return 1 ;}
		</dev/null $SSH -q -o BatchMode=yes -o ConnectTimeout=1 ${UNAME}@$ip :
		return $?
		;;
	term)
		_running $vid || { echo $(_shortname $vid) not running; return 1 ;}
		</dev/tty $SSH ${UNAME}@$ip "$@" >/dev/tty 2>&1 ;;
	module) # mod name -> args -> (Exec, Output) -- shorthand kinda
		_running $vid || { echo $(_shortname $vid) not running; return 1 ;}
		mname=$1; shift
		_exec $vid $SH_SHARE/module/${mname}.sh "$@"
		;;
	pin)  # (stdin) -> file -> args -> (Exec,Output)
		_running $vid || { echo $(_shortname $vid) not running; return 1 ;}
		_exec $vid $tmp/stdin "$@"
		;;
	script) # ex file /my/path -n foo other args
		_running $vid || { echo $(_shortname $vid) not running; return 1 ;}
		test -f "$1" || return 1
		_exec $vid "$@"
		;;
	cmd) # run blind
		_running $vid || { echo $(_shortname $vid) not running; return 1 ;}
		if test -s $tmp/stdin
		then <$tmp/stdin $SSH ${UNAME}@$ip "$@"
		else $SSH ${UNAME}@$ip "$@"
		fi
		;;
	esac
}

DONE=true
_process_no_vm "$@" || exit $?
$DONE && return 0
_with state vmset
_search_id $QH/vms name "$name" | _cmd_state vmset update vms
_cmd_state vmset extant vms || _error no vms match "$name"

test -t 0 || cat - >$tmp/stdin
for vid in $(_cmd_state vmset read vms)
do
	ip=$(_addr $vid) || { echo no ip available; exit $? ;}
	_process_vm $vid "$@" || exit $?
done
