#!/bin/sh
. ${SH_SHARE}/composite/test/.init.sh
while getopts h arg
do
    case "$arg" in
    h) _comp_gen_help && exit 0 ;; # help
    esac
done; unset arg; shift $(( $OPTIND - 1 )); OPTIND=1
_kick type-test

_init () {
	:
}

_test__git () {
	cat >$tmp/repos <<EOF
st git://git.suckless.org/st
# gift git@gitlab.com:setchell.dave/gift.git
# qtree git@gitlab.com:setchell.dave/qtree.git
# shlib git@gitlab.com:setchell.dave/shlib
kore https://git.kore.io/kore.git
abduco https://github.com/martanne/abduco.git
dvtm https://github.com/martanne/dvtm.git
EOF
		while read REPLY
		do
			test "$(echo $REPLY | cut -c1)" = "#" && continue
			</dev/null _cmd_out type-test type git -r ${REPLY#* } -p ${REPLY%% *} dir
		done <$tmp/repos
}


_test_user () {
	:
}
	
_test__foo () {
	_cmd_out type-test module net nic
	_cmd_out type-test -u dave type user home
}


. ${SH_SHARE}/composite/.exe.sh
case $cmd in
init) _init ;;
all)
    for tname in $(_tests)
    do
        _test_wrapper $tname
    done
    ;;
*) _test_wrapper $cmd ;;
esac

