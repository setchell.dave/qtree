#!/bin/sh
. ${SH_SHARE}/composite/test/.init.sh

while getopts h arg
do
    case "$arg" in
    h) _comp_gen_help && exit 0 ;; # help
    esac
done; unset arg; shift $(( $OPTIND - 1 )); OPTIND=1
D="$V -n ${name}/main"

_comp_list () {
	echo main
}

_init () {
	:
}

_test__list () {
	$D list
}

_test__available () {
	$D available
}

_test__conf () {
	$D conf all one
	$D conf dump one
	$D conf remove one
}

_test__self () {
	$D self sh echo from self test: $PWD
	$D self module net
}

_test__stats () {
	$D stats
}

_test__module () {
	$D module net
	$D module disk
}

_test__sh () {
	$D sh hostname
	$D sh ip addr
}

. ${SH_SHARE}/composite/.exe.sh
case $cmd in
all)
    for tname in $(_tests)
    do
        _test_wrapper $tname
    done
    ;;
*) _test_wrapper $cmd ;;
esac

