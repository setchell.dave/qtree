QTREE_VERSION=1
. ${SH_SHARE}/composite/.init.sh

while getopts hkisc arg
do
    case "$arg" in
    h) _gen_help && exit 0 ;;
    k) KEEP=true ;;
    c) COLUMN=_col ;;
    i) INTER=true ;;
    s) SHOWCMD=false ;;
    esac
done; unset arg; shift $(( $OPTIND - 1 )); OPTIND=1
: ${KEEP:=false}
: ${INTER:=false}
: ${COLUMN:=cat}
: ${SHOWCMD:=true}

_test_wrapper () {
    t="$1"; shift
    printf 'starting test [ %s ]' $t | _box
    </dev/tty _test__${t} "$@" >/dev/tty 2>/dev/tty || { KEEP=true; _error failed test $t ;}
}

_pre_clean () {
    $KEEP && return 0
    for i in $(_comp_list)
    do
		$V -n ${name}/$i stop >/dev/tty && $V -n ${name}/$i destroy >/dev/tty || _error failed ma doods
	done
    $V silence
    $V clean
}

_cmd_out () {
    $SHOWCMD && _show "$@" | _underline
    $V "$@" | $COLUMN
	$INTER && { _ask 'you want in? ' && _do $1 sh ;}
}

_tests () {
    _gen_help | sed -n '/^FUNCTION.*_test__/ s/^.*_test__//p' | cut -d' ' -f1
}
