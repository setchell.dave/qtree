#!/bin/sh
. ${SH_SHARE}/composite/test/.init.sh

while getopts h arg
do
    case "$arg" in
    h) _comp_gen_help && exit 0 ;; # help
    esac
done; unset arg; shift $(( $OPTIND - 1 )); OPTIND=1
_kick mod-test

_init () {
	_real_stop mod-test
	_do mod-test disk-num 2
	_do mod-test nic-num 2
	_do mod-test start
}


_test__disk () {
	# transitively tests: format
	for d in /dev/vdb /dev/vdc
	do
		_cmd_out mod-test module disk -d $d mount
		_cmd_out mod-test module disk -d $d persist
	done
	_cmd_out mod-test module path -f -p /etc/fstab dump
	_cmd_out mod-test module disk ls
	_cmd_out mod-test module disk actual
	_cmd_out mod-test module disk stats
}

_test__path () {
	pt=${tmp}/path_test
	mkdir ${pt}
	for d in aaa bbb cc
	do
		for dd in xxx yyy zzz
		do
			mkdir -p ${pt}/${d}/${dd}
			touch ${pt}/${d}/${dd}/HELO
			touch ${pt}/${d}/${dd}/$(_r)
		done
	done
	$V self module path -p ${pt} dump | _cmd_out mod-test module path -H foo place
	_cmd_out mod-test module path -H foo stats
	_cmd_out mod-test module path -f -H foo/aaa/xxx/HELO place <<EOF
first non empty

1
EOF
	_cmd_out mod-test module path -f -H foo/aaa/xxx/HELO dump
	$V self module path -p ${pt} dump | _cmd_out mod-test module path -H foo place
	_cmd_out mod-test module path -f -H foo/aaa/xxx/HELO place <<EOF
second non empty

2
EOF
	_cmd_out mod-test module path -f -H foo/aaa/xxx/HELO dump
	$V self module path -p ${pt} dump | _cmd_out mod-test module path -H foo place
	_cmd_out mod-test module path -f -H foo/aaa/xxx/HELO place <<EOF
third non empty

3
EOF
	_cmd_out mod-test module path -f -H foo/aaa/xxx/HELO dump
	_cmd_out mod-test module path -H foo restore
	_cmd_out mod-test module path -f -H foo/aaa/xxx/HELO dump
	_cmd_out mod-test module path -H foo restore 2
	_cmd_out mod-test module path -f -H foo/aaa/xxx/HELO dump
	_cmd_out mod-test module path -f -H foo/f1 place <<EOF
I'm a file guys
srsly. care about me.
EOF
	_cmd_out mod-test module path -f -H foo/f1 place <<EOF
even more to say to you
we will never run out of away
EOF
	_cmd_out mod-test module path -f -H foo/f1 dump
	_cmd_out mod-test module path -f -H foo/f1 restore
	_cmd_out mod-test module path -f -H foo/f1 dump
	_cmd_out mod-test module path -H foo backups
}

_test__net () {
	_cmd_out mod-test module net stats
	_cmd_out mod-test module net idem_br br0 192.168.200.0/24 
	_cmd_out mod-test module net add_tap br0 3 root
	_cmd_out mod-test module net stats
	_cmd_out mod-test module net destroy_br br0 192.168.200.0/24
	_cmd_out mod-test module net stats
}

_test__pkg () {
	_cmd_out mod-test module pkg stats
	_cmd_out mod-test module pkg list
}

. ${SH_SHARE}/composite/.exe.sh
case $cmd in
init) _init ;;
all)
    for tname in $(_tests)
    do
        _test_wrapper $tname
    done
    ;;
*) _test_wrapper $cmd ;;
esac

