#!/bin/sh

_validate
DONE=true
: ${cmd:=${1:-stats}}; test -n "$1" && shift
case "$cmd" in
init) _init ;; # () -> Monad instances initialized
list) _comp_list ;;
do)
	nmatch="$1"; shift
	for i in $(_comp_list | grep "$nmatch")
	do $V -n $i "$@"; done
	;;
stats) # () -> text::instance-stats
    for i in $(_comp_list)
    do
        $VR -n $i stats | _box
    done
    ;;
stop|start) _comp_list | xargs -Ixx $V -n xx $cmd ;;
*) DONE=false ;;
esac
$DONE && exit 0
