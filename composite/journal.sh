#!/bin/sh
QTREE_VERSION=1
. ${SH_SHARE}/composite/.init.sh
CTYPE=journal

while getopts n:hd:c: arg
do
    case "$arg" in
    h) _comp_gen_help && exit 0 ;; # help
    d) DB="$OPTARG" ;; # set explicit db name
    c) CODE="$OPTARG" ;; # set explicit code name
	z) TZONE="$OPTARG" ;; # set timezone US/Pacific by default
    esac
done; unset arg; shift $(( $OPTIND - 1 )); OPTIND=1
: ${DB:=db}
: ${CODE:=code}
: ${TZONE:=US/Pacific}

_comp_list () {
	printf '%s\n' "${name}/$DB" "${name}/$CODE"
}

_init () {  # initialize comp
    $V -n ${name}/$CODE type dev init
	$V -n ${name}/$CODE pin <<'EOF'
. ${HOME}/.mkshrc
. ${HOME}/.profile
mkdir -p ${HOME}/repo; cd ${HOME}/repo
test -e journal || git clone git@gitlab.com:setchell.dave/journal
cd journal
git submodule init
git submodule update --remote
go get github.com/jmhodges/jsonpp
curl -L https://yt-dl.org/downloads/latest/youtube-dl -o youtube-dl
chmod a+rx youtube-dl
EOF
    $VR -n ${name}/$DB type pg init
    $VR -n ${name}/$DB type pg conn_str \
        | $V -n ${name}/$CODE module path -f -p '${HOME}/repo/journal/pg.conn' replace
}

. ${SH_SHARE}/composite/.exe.sh
