#!/bin/sh
QTREE_VERSION=1
. ${SH_SHARE}/composite/.init.sh
CTYPE=blank

while getopts n:hc: arg
do
    case "$arg" in
    h) _genHelp && exit 0 ;; # help
    c) count=$OPTARG ;;
    esac
done; unset arg; shift $(( $OPTIND - 1 )); OPTIND=1

_comp_list () {
	: ${count:=${1:-3}}; test $count -gt 10 && count=10
	printf '%s\n' ichi ni san shi go roku shichi hachi kyuu juu | sed -n "1,${count} s#^#${name}/#p"
}


_init () {  # initialize comp
	:
}

. ${SH_SHARE}/composite/.exe.sh
