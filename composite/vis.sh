#!/bin/sh
QTREE_VERSION=1
. ${SH_SHARE}/composite/.init.sh

while getopts c:h arg
do
    case "$arg" in
    h) _genHelp && exit 0 ;; # help
    c) CODE="$OPTARG" ;;
    esac
done; unset arg; shift $(( $OPTIND - 1 )); OPTIND=1
ctype=$(basename $0); ctype=${ctype%.sh}
: ${CODE:=code}
_kick $CODE

_init () {  # initialize comp
    _do $CODE -u root module pkg repo community
    _do $CODE -u root module pkg add vis curl
    cat $HOME/.ssh/*.pub | _do $CODE type user loadKeys
    GD=$(_do $CODE type git -p vis -r https://github.com/martanne/vis.git dir)
    _do $CODE sh mkdir -p "${GD}/ex_libs"
    _do $CODE cd "${GD}/ex_libs" "curl -O \
        http://www.leonerd.org.uk/code/libtermkey/libtermkey-0.22.tar.gz"
    _do $CODE cd "${GD}/ex_libs" "tar -xzf libterm*.tar.gz"
}


. ${SH_SHARE}/composite/.exe.sh
