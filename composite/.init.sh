#!/bin/sh
. ${SH_SHARE}/.func.sh
. ${SH_SHARE}/.msg.sh
VR="$V -u root"

_comp_list () {
	:
}

_validate () { # () -> bool
	_comp_list | xargs -Ixx $VR -n xx start
	doit=false
	for i in $(_comp_list)
	do
	    rv=$($VR -n $i module path -f -p /var/qtree/compv/${CTYPE}.v dump)
	    test "$QTREE_VERSION" -gt ${rv:-0} && { doit=true; break ;}
	done
	$doit && _init && {
		for i in $(_comp_list)
		do
		    echo $QTREE_VERSION \
				| $VR -n $i module path -f -p /var/qtree/compv/${CTYPE}.v replace
		done ;}
			
}

_comp_gen_help () {
	_gen_help ${SH_SHARE}/composite/.init.sh
	_gen_help ${SH_SHARE}/composite/.exe.sh
	_gen_help
}

