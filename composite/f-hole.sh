#!/bin/sh
QTREE_VERSION=1
. ${SH_SHARE}/composite/.init.sh

while getopts a:h arg
do
    case "$arg" in
    h) _genHelp && exit 0 ;; # help
    a) APP="$OPTARG" ;;
    esac
done; unset arg; shift $(( $OPTIND - 1 )); OPTIND=1
ctype=$(basename $0); ctype=${ctype%.sh}
: ${APP:=app}
_kick $APP

_init () {
    _do $APP -u root module pkg repo community 
    _do $APP -u root module pkg add tree vis python3 libpq libevent bsd-compat-headers openssl-dev
    KD=$(_do $APP type git -p kore -r https://git.kore.io/kore.git dir)
    KDD=$(_do $APP type git -p kore-doc -r https://git.kore.io/kore-doc.git dir)
    FD=$(_do $APP type git -p f-hole -r git@gitlab.com:setchell.dave/f-hole.git dir)
    _do $APP cd $KD make
    _do $APP cd $KD make
    _do $APP -u root cd $KD make install
    echo $KD $FD
}

. ${SH_SHARE}/composite/.exe.sh
