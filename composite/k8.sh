#!/bin/sh
QTREE_VERSION=1
. ${SH_SHARE}/composite/.init.sh

while getopts n:h arg
do
    case "$arg" in
    h) _genHelp && exit 0 ;; # help
    esac
done; unset arg; shift $(( $OPTIND - 1 )); OPTIND=1
ctype=$(basename $0); ctype=${ctype%.sh}
_kick master
_kick node_01
_kick node_02
_kick node_03

_init () {  # initialize comp
    for nn in $(_list)
    do
        _do $nn -u root module pkg -e repo main
        _do $nn -u root module pkg -e repo community
        _do $nn -u root module pkg -e repo testing
        _do $nn -u root module pkg update
        _do $nn -u root sh modprobe br_netfilter
        _do $nn -u root module pkg add kubernetes containerd go iproute2 ethtool
    done
    _do master -u root module pkg add git musl-dev
    _do master -u root sh 'echo 1 >/proc/sys/net/ipv4/ip_forward'
    _do master -u root sh 'echo 1 >/proc/sys/net/bridge/bridge-nf-call-iptables'
    _do master -u root sh go get github.com/kubernetes-incubator/cri-tools/cmd/crictl
}

. ${SH_SHARE}/composite/.exe.sh
