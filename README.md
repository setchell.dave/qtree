# qtree
needed: 
    alpine:
        busybox-extra
        procps
        util-linux
        qemu-img
        qemu-system-x86_64
        iproute2
	ncurses
	



are things defined just by code? Think maybe they should be. 
    This means there is no stored state nor state file format, like json. 
    There is only the state on the disk that represents the instances themselves
    And the commands to create new states. i.e. for lxc containers nothing
    is stored. When you `execute lxc -n foo start` it creates a new lxc.
    determining all the lxc's out there is running `execute lxc list` on all
    instances. Storing the config for those lxc containers is simply
    storing the commands, in any way you see fit, to create them. 
    This way an lxc with say postgres and some db creation stuff is simply
    Something like ( written before interface fully designed ):

#!/bin/sh

vm.sh -n foo execute lxc -n pg01 start
pg01_ip=$(vm.sh -n foo execute lxc -n pg01 ip)
vm.sh -n $pg01_ip connect apk add postgresql
vm.sh -n $pg01_ip connect sed \
    "s/listen_address=[^[:space:]]*/listen_address="$ip/" /etc/postgresql.conf
<initdb.sql vm.sh -n $pg01_ip connect psql testdb
...

This allows no need for representing all kinds of shit. like some defined structure
for postgres options or disk configs or other shit that sucks and expires real quick like. 

Just use direct commands. The interface, even if comparatively volatile, is easier to maintain. 
Also replication is *really* just setting variables. It's a fucking program: it does variables. 
Config files just do over complicated lookups. it works because (math)functions are isomorphic
with key->value maps. config files ( puppet et al ) are maps; that representing doing. 
functions are maps that do. 


